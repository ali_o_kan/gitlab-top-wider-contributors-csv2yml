# Top wider community contributors csv2yaml

## Get data CSV

1. goto [bitergia Merged Community MRs](https://gitlab.biterg.io/goto/3fe8a6a572aeae3b0fb60d26b18599bd)
1. Update `merged_at` filter
1. Edit visualization in Submitters options: 
    - Add `sub-buckets` < `Split Rows`: `Sub Aggregation=Terms`, `Field=author_username`, `Custom Label=gitlab`
    - Rename `Submitter` to `name` 
    - Rename `Merge Requests` to `merged_mrs`
    - disable `Repositories` and `Avg. Open Days`
1. Download Raw

## CSV to YAML

1. put `gitlab-merges_submitters.csv` in root folder 
1. `ruby script.rb`