require 'csv'
require 'yaml'

bots = [
    "gitlab-bot",
    "gitlab-release-tools-bot",
    "gitlab-dependency-bot",
    "group_2750817_bot",
    "gitlab-crowdin-bot",
    "gitlab-dependency-update-bot",
    "employment-bot",
]

source = CSV.table('gitlab-merges_submitters.csv')

# remove less than 5 mrs or GitLab bots
source.delete_if do |row|
    row[:merged_mrs] <5 || bots.include?(row[:gitlab].to_s)
end
  
formatted = source.map do |row| 
    hash = row.to_hash
    hash[:gitlab] = "https://gitlab.com/" + hash[:gitlab].to_s
    hash[:category] = if hash[:merged_mrs] > 75
        "SuperStar"
    elsif hash[:merged_mrs] > 10
        "Star"
    else
        "Enthusiast"
    end
    
    Hash[hash.map { |key, value| [key.to_s, value] }]
end

puts formatted.to_yaml

File.write('top_contributors_2022.yml', formatted.to_yaml)
